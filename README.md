# lfs-check

Checks supplied files are under LFS control or not

# Usage

`lfs-check <glob>`

Zsh style glob was allowed (i.e. `lfs-check "**/*.png")
