module lfs_check

require (
	github.com/alcortesm/tgz v0.0.0-20161220082320-9c5fe88206d7 // indirect
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239 // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emirpasic/gods v1.9.0 // indirect
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/gliderlabs/ssh v0.1.1 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/kevinburke/ssh_config v0.0.0-20180830205328-81db2a75821e // indirect
	github.com/mattn/go-zglob v0.0.0-20180803001819-2ea3427bfa53
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/pelletier/go-buffruneio v0.2.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/src-d/gcfg v1.3.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/xanzy/ssh-agent v0.2.0 // indirect
	go.etcd.io/bbolt v1.3.0
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793 // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd // indirect
	golang.org/x/sys v0.0.0-20180907202204-917fdcba135d // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/src-d/go-billy.v4 v4.2.1 // indirect
	gopkg.in/src-d/go-git-fixtures.v3 v3.1.1 // indirect
	gopkg.in/src-d/go-git.v4 v4.7.0
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
