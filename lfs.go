/*
 * Copyright (c) 2018  Masashi Fujita <objectxtreme@gmail.com>.  All rights reserved.
 */

package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
	"go.etcd.io/bbolt"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

var (
	lfsBucketName = []byte("lfs")
)

// LFSItem hold the LFS information.
type LFSItem struct {
	Path string
	Hash *[sha256.Size]byte
}

func (l LFSItem) String() string {
	return fmt.Sprintf("LFSItem { \"%s\", \"%s\" }", l.Path, hex.EncodeToString(l.Hash[:]))
}

func getLFSItems() ([]LFSItem, error) {
	var err error
	cmd := exec.Command("git", "lfs", "ls-files", "--long")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	err = cmd.Start()
	if err != nil {
		return nil, err
	}
	scanner := bufio.NewScanner(stdout)
	var result []LFSItem
	for scanner.Scan() {
		l := scanner.Text()
		fields := strings.Split(l, " ")
		if len(fields) != 3 {
			continue
		}
		//fmt.Fprintf(os.Stderr, "%s\t%s\n", fields[0], fields[2])
		h, err := toBytes(fields[0])
		if err != nil {
			return nil, err
		}
		item := LFSItem{Path: fields[2], Hash: &[sha256.Size]byte{}}
		copy(item.Hash[:], h[:sha256.Size])
		result = append(result, item)
	}
	if err = cmd.Wait(); err != nil {
		return nil, err
	}
	return result, nil
}

func toBytes(s string) ([]byte, error) {
	if len(s) != (2 * sha256.Size) {
		return nil, errors.Errorf("bad string length (%d but expected %d)", len(s), sha256.Size)
	}
	b, err := hex.DecodeString(s)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// CreateLFSDatabase creates LFS controlled file database.
func CreateLFSDatabase(tx *bolt.Tx, force bool) error {
	var err error
	curH, err := getHeadCommitHash()
	if err != nil {
		return errors.Wrap(err, "failed to obtain HEAD commit hash.")
	}
	if !force {
		b := tx.Bucket([]byte("git"))
		if b != nil {
			h := b.Get([]byte("hash"))
			Verboseln("previous hash = %v, current = %v", hex.EncodeToString(h), curH)
			if h != nil {
				if compareHash(curH[:], h) {
					return nil
				}
			}
		}
		Verboseln("Hash mismatched")
	}
	{
		b, err := tx.CreateBucketIfNotExists([]byte("git"))
		if err != nil {
			return errors.Wrap(err, "failed to create `git` bucket")
		}
		err = b.Put([]byte("hash"), curH[:])
		if err != nil {
			return errors.Wrap(err, "failed to save current commit hash")
		}
		Verboseln("Head commit hash: %v", curH)
	}
	items, err := getLFSItems()
	if err != nil {
		return err
	}
	err = tx.DeleteBucket(lfsBucketName)
	if err != nil {
		if err != bolt.ErrBucketNotFound {
			return err
		}
	}
	bucket, err := tx.CreateBucket(lfsBucketName)
	if err != nil {
		return err
	}
	for _, v := range items {
		//fmt.Fprintf(os.Stdout, "%v\n", v)
		err = bucket.Put([]byte(v.Path), v.Hash[:])
	}
	return nil
}

func compareHash(a []byte, b []byte) bool {
	if a == nil {
		return b == nil
	}
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func getHeadCommitHash() (plumbing.Hash, error) {
	var err error
	r, err := git.PlainOpenWithOptions(".", &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return plumbing.ZeroHash, err
	}
	head, err := r.Head()
	if err != nil {
		return plumbing.ZeroHash, err
	}
	return head.Hash(), nil
}

// LFSResult holds the query result of `checkLFS`.
type LFSResult struct {
	Path  string
	IsLFS bool
}

func checkLFS(tx *bolt.Tx, paths []string) ([]LFSResult, error) {
	bucket := tx.Bucket(lfsBucketName)
	if bucket == nil {
		return nil, errors.Errorf("Missing `lfs` bucket")
	}
	result := make([]LFSResult, 0, len(paths))
	for _, p := range paths {
		v := bucket.Get([]byte(p))
		if v == nil {
			result = append(result, LFSResult{Path: p, IsLFS: false})
		} else {
			result = append(result, LFSResult{Path: p, IsLFS: true})
		}
	}
	return result, nil
}
