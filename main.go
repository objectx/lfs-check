/*
 * Copyright (c) 2018  Masashi Fujita <objectxtreme@gmail.com>.  All rights reserved.
 */

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"github.com/mattn/go-zglob"
	"github.com/pkg/errors"
	"go.etcd.io/bbolt"
)

var (
	progPath  = getProgramPath("lfs-check")
	progName  = filepath.Base(progPath)
	beVerbose = false

	// DatabaseName is the database file name.
	DatabaseName = "lfs-check.db"
)

func main() {
	var err error
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n", progName)
		flag.PrintDefaults()
		os.Exit(1)
	}
	flag.BoolVar(&beVerbose, "v", false, "Be verbose")
	force := flag.Bool("force", false, "Force updating database")
	flag.Parse()
	dbFile, err := getDatabaseFileName(".")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s:error: %v", progName, err)
		os.Exit(1)
	}
	err = (func() error {
		db, err := bolt.Open(dbFile, 0666, nil)
		if err != nil {
			return err
		}
		defer db.Close()
		return db.Update(func(tx *bolt.Tx) error {
			return CreateLFSDatabase(tx, *force)
		})
	})()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s:error: %v", progName, err)
		os.Exit(1)
	}
	expandedArgs, err := (func(args []string) ([]string, error) {
		result := make([]string, 0, len(args))
		for _, a := range args {
			expanded, err := zglob.Glob(a)
			if err != nil {
				return nil, err
			}
			result = append(result, expanded...)
		}
		return result, nil
	})(flag.Args())
	err = (func() error {
		db, err := bolt.Open(dbFile, 0666, &bolt.Options{ReadOnly: true})
		if err != nil {
			return err
		}
		defer db.Close()
		return db.View(func(tx *bolt.Tx) error {
			result, err := checkLFS(tx, expandedArgs)
			if err != nil {
				return err
			}
			for _, item := range result {
				if item.IsLFS {
					fmt.Fprintf(os.Stdout, "    LFS: %s\n", item.Path)
				} else {
					fmt.Fprintf(os.Stdout, "NOT LFS: %s\n", item.Path)
				}
			}
			return nil
		})
	})()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s:error: %v", progName, err)
		os.Exit(1)
	}
	os.Exit(0)
}

func getProgramPath(def string) string {
	p, err := os.Executable()
	if err == nil {
		return p
	}
	return def
}

// Verboseln show verbose message.
func Verboseln(format string, args ...interface{}) {
	if beVerbose {
		fmt.Fprintf(os.Stderr, "%s: ", progName)
		fmt.Fprintf(os.Stderr, format, args...)
		fmt.Fprintln(os.Stderr)
	}
}

func getProjectRootDir(startDir string) (string, error) {
	var err error
	d, err := filepath.Abs(startDir)
	if err != nil {
		return "", err
	}
	for {
		gitDir := filepath.Join(d, ".git")
		Verboseln("Looking for %s", gitDir)
		_, err = os.Stat(gitDir)
		if err == nil {
			return d, nil
		}
		up := filepath.Dir(d)
		if up == d {
			return "", errors.Errorf("failed to find `.git`")
		}
		d = up
	}
}

func getDatabaseFileName(startDir string) (string, error) {
	root, err := getProjectRootDir(startDir)
	if err != nil {
		return "", err
	}
	return filepath.Join(root, ".git", DatabaseName), nil
}
